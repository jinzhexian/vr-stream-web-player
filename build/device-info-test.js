(function(){function r(e,n,t){function o(i,f){if(!n[i]){if(!e[i]){var c="function"==typeof require&&require;if(!f&&c)return c(i,!0);if(u)return u(i,!0);var a=new Error("Cannot find module '"+i+"'");throw a.code="MODULE_NOT_FOUND",a}var p=n[i]={exports:{}};e[i][0].call(p.exports,function(r){var n=e[i][1][r];return o(n||r)},p,p.exports,r,e,n,t)}return n[i].exports}for(var u="function"==typeof require&&require,i=0;i<t.length;i++)o(t[i]);return o}return r})()({1:[function(require,module,exports){
class DeviceInfo {
	constructor({manufacturer, model, revision, deviceClass, deviceId}) {
		this.manufacturer = manufacturer;
		this.model = model;
		this.revision = revision;
		this.deviceClass = deviceClass;
		this.deviceId = deviceId;
	}

	get Manufacturer() {
		return this.manufacturer;
	}

	get Model() {
		return this.model;
	}

	get Revision() {
		return this.revision;
	}

	get DeviceClass() {
		return this.deviceClass;
	}

	get DeviceId() {
		return this.deviceId;
	}

	get SensorSpec() {
		return this.spec;
	}

	set SensorSpec(spec) {
		this.spec = spec;
	}

	toJson(pretty) {
		if (pretty) {
			return JSON.stringify(this, null, '  ');
		}
		return JSON.stringify(this);
	}
}

module.exports = DeviceInfo;

},{}],2:[function(require,module,exports){
var DeviceInfo = require('../src/deviceinfo.js');

var di = new DeviceInfo();
var centroid = di.getLeftEyeCenter();

// Size the canvas. Render the centroid.
var canvas = document.querySelector('canvas');
var w = window.innerWidth;
var h = window.innerHeight;
var x = centroid.x * w/2;
var y = centroid.y * h;
var size = 10;

canvas.width = w;
canvas.height = h;

var ctx = canvas.getContext('2d');
ctx.clearRect(0, 0, w, h);
ctx.fillStyle = 'black';
ctx.fillRect(x - size/2, y - size/2, size, size);

console.log('Placing eye at (%d, %d).', x, y);

},{"../src/deviceinfo.js":1}]},{},[2]);
